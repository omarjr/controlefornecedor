
package visao;

import control.ControleFornecedor;
import control.ControleProduto;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.ListSelectionModel;
import model.Endereco;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import model.Produto;
import javax.swing.table.DefaultTableModel;

public class CadastroFornecedor extends JFrame {
    
    int variavel;
    private Fornecedor fornecedor;
    private ControleFornecedor controleFornecedor;
    private ControleProduto controleProduto;
    private PessoaFisica pessoaFisica;
    private PessoaJuridica pessoaJuridica;
    private Endereco endereco;
    private Produto produto;
    private boolean modoAlteracao;
    private boolean novoRegistro;
    private DefaultListModel telefonesListModel;

    public CadastroFornecedor() {
        initComponents();
        this.habilitarDesabilitarCampos();
        this.telefonesListModel = new DefaultListModel();
        this.jListListaTelefones.setModel(telefonesListModel);
        this.controleFornecedor = new ControleFornecedor();
        this.controleProduto = new ControleProduto();
        this.jTableListaFornecedores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.jTableListaProdutos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private void limparCampos(){
        jTextFieldNome.setText(null);
        jTextFieldCpf.setText(null);
        jTextFieldCnpj.setText(null);
        jTextFieldRazaoSocial.setText(null);
        jTextFieldLogradouro.setText(null);
        jTextFieldNumero.setText(null);
        jTextFieldBairro.setText(null);
        jTextFieldCidade.setText(null);
        jComboBoxEstado.setSelectedIndex(0);
        jTextFieldPais.setText(null);
        jTextFieldComplemento.setText(null);
        /*jTextFieldNomeProduto.setText(null);
        jTextFieldDescricaoProduto.setText(null);
        jTextFieldValorVenda.setText(null);
        jTextFieldValorCompra.setText(null);
        jTextFieldQuantidadeEstoque.setText(null);*/
    }
    
    private void preencherCampos(){
        
        ArrayList<String> telefones;
        
        if(fornecedor.getIndentificador().equals("pFisica")) {
            
            PessoaFisica pessoaFisica = (PessoaFisica) fornecedor;
            jTextFieldNome.setText(pessoaFisica.getNome());      
            jTextFieldCpf.setText(pessoaFisica.getCpf());
            jTextFieldCnpj.setText("");
            jTextFieldRazaoSocial.setText("");
            
                    jTextFieldLogradouro.setText(pessoaFisica.getEndereco().getLogradouro());
                    jTextFieldNome.setText(pessoaFisica.getNome());
                    jTextFieldNumero.setText(pessoaFisica.getEndereco().getNumero());
                    jTextFieldPais.setText(pessoaFisica.getEndereco().getPais());
                    jTextFieldBairro.setText(pessoaFisica.getEndereco().getBairro());
                    jTextFieldCidade.setText(pessoaFisica.getEndereco().getCidade());
                    jTextFieldComplemento.setText(pessoaFisica.getEndereco().getComplemento());

            /*jTextFieldLogradouro.setText(endereco.getLogradouro());
            jTextFieldNumero.setText(endereco.getNumero());
            jTextFieldBairro.setText(endereco.getBairro());
            jTextFieldCidade.setText(endereco.getCidade());
            jComboBoxEstado.setSelectedItem(endereco.getEstado());
            jTextFieldPais.setText(endereco.getPais());
            jTextFieldComplemento.setText(endereco.getComplemento());

            /*jTextFieldNomeProduto.setText(produto.getNomeProduto());
            jTextFieldDescricaoProduto.setText(produto.getDescricaoProduto());

            Double valorCompraDouble = Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompraDouble);

            Double valorVendaDouble = Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVendaDouble);

            Double valorQuantidadeEstoqueDouble = Double.parseDouble(jTextFieldQuantidadeEstoque.getText());
                produto.setQuantidadeEstoque(valorQuantidadeEstoqueDouble);

            jTextFieldValorCompra.setText(Double.toString(produto.getValorCompra()));
            jTextFieldValorVenda.setText(Double.toString(produto.getValorVenda()));
            jTextFieldNomeProduto.setText(Double.toString(produto.getQuantidadeEstoque()));*/

            telefonesListModel.clear();
            telefones = pessoaFisica.getTelefone();
            for (String t : telefones) {
                telefonesListModel.addElement(t);
            }
        
        }
        
        else {
            PessoaJuridica pessoaJuridica = (PessoaJuridica) fornecedor;
            
            jTextFieldNome.setText(pessoaJuridica.getNome());      
            jTextFieldCpf.setText("");
            jTextFieldCnpj.setText(pessoaJuridica.getCnpj());
            jTextFieldRazaoSocial.setText(pessoaJuridica.getRazaoSocial());

            jTextFieldLogradouro.setText(pessoaJuridica.getEndereco().getLogradouro());
            jTextFieldNumero.setText(pessoaJuridica.getEndereco().getNumero());
            jTextFieldBairro.setText(pessoaJuridica.getEndereco().getBairro());
            jTextFieldCidade.setText(pessoaJuridica.getEndereco().getCidade());
            jComboBoxEstado.setSelectedItem(pessoaJuridica.getEndereco().getEstado());
            jTextFieldPais.setText(pessoaJuridica.getEndereco().getPais());
            jTextFieldComplemento.setText(pessoaJuridica.getEndereco().getComplemento());

            /*jTextFieldNomeProduto.setText(produto.getNomeProduto());
            jTextFieldDescricaoProduto.setText(produto.getDescricaoProduto());

            Double valorCompraDouble = Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompraDouble);

            Double valorVendaDouble = Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVendaDouble);

            Double valorQuantidadeEstoqueDouble = Double.parseDouble(jTextFieldQuantidadeEstoque.getText());
                produto.setQuantidadeEstoque(valorQuantidadeEstoqueDouble);

            jTextFieldValorCompra.setText(Double.toString(produto.getValorCompra()));
            jTextFieldValorVenda.setText(Double.toString(produto.getValorVenda()));
            jTextFieldNomeProduto.setText(Double.toString(produto.getQuantidadeEstoque()));*/

            telefonesListModel.clear();
            telefones = pessoaJuridica.getTelefone();
            for (String t : telefones) {
                telefonesListModel.addElement(t);
            }
        
        }
              
    }
    
        private boolean validarCampos(){
        if (jLabelNome.getText().trim().length() == 0){
            this.exibirInformacao("O valor do campo 'Nome' não foi informado.");
            jLabelNome.requestFocus();
            return false;
        }
        return true;
    }
    
   
    private void habilitarDesabilitarCampos(){
        boolean registroSelecionado = (fornecedor != null);
        
        jCheckBoxPessoaFisica.setEnabled(modoAlteracao);
        jCheckBoxPessoaJuridica.setEnabled(modoAlteracao);
        jTextFieldNome.setEnabled(modoAlteracao);
        jTextFieldCpf.setEnabled(modoAlteracao);
        jTextFieldCnpj.setEnabled(modoAlteracao);
        jListListaTelefones.setEnabled(modoAlteracao);
        jTextFieldRazaoSocial.setEnabled(modoAlteracao);
        jTextFieldLogradouro.setEnabled(modoAlteracao);
        jTextFieldNumero.setEnabled(modoAlteracao);
        jTextFieldBairro.setEnabled(modoAlteracao);
        jTextFieldCidade.setEnabled(modoAlteracao);
        jComboBoxEstado.setEnabled(modoAlteracao);
        jTextFieldPais.setEnabled(modoAlteracao);
        jTextFieldComplemento.setEnabled(modoAlteracao);
        /*jTextFieldNomeProduto.setEnabled(modoAlteracao);
        jTextFieldDescricaoProduto.setEnabled(modoAlteracao);
        jTextFieldValorCompra.setEnabled(modoAlteracao);
        jTextFieldValorVenda.setEnabled(modoAlteracao);
        jTextFieldQuantidadeEstoque.setEnabled(modoAlteracao);*/
        jButtonCancelar.setEnabled(modoAlteracao);
        jButtonEditar.setEnabled(modoAlteracao == false && registroSelecionado == true);
        jButtonExcluir.setEnabled(modoAlteracao == false && registroSelecionado == true);
        jButtonNovo.setEnabled(modoAlteracao == false);
        jButtonPesquisar.setEnabled(modoAlteracao == false);
        jButtonSalvar.setEnabled(modoAlteracao);
        jButtonAdicionarTelefone.setEnabled(modoAlteracao);
        jButtonRemoverTelefone.setEnabled(modoAlteracao);
        jButtonAdicionarProduto.setEnabled(modoAlteracao);
        jButtonRemoverProduto.setEnabled(modoAlteracao);
        jTableListaProdutos.setEnabled(modoAlteracao);
        jTableListaFornecedores.setEnabled(modoAlteracao == false);
        
        
        
    }
    
    private void salvarRegistro(){
        
        Endereco endereco = new Endereco();
        Produto produto = new Produto();
        ArrayList<String> telefones;
        
        
        if (this.validarCampos() == false) {
            return;   
        }
        
            if(variavel == 1){
                
                if (novoRegistro == true){
                    pessoaFisica = new PessoaFisica(jTextFieldNome.getText(), jTextFieldCpf.getText());
                }
                else{
                     pessoaFisica.setNome(jTextFieldNome.getText());
                     pessoaFisica.setCpf(jTextFieldCpf.getText());
                }
               
                
               
                endereco.setLogradouro(jTextFieldLogradouro.getText());
                endereco.setNumero(jTextFieldNumero.getText());
                endereco.setBairro(jTextFieldBairro.getText());
                endereco.setCidade(jTextFieldCidade.getText());
                endereco.setPais(jTextFieldPais.getText());
                endereco.setComplemento(jTextFieldComplemento.getText());

                pessoaFisica.setEndereco(endereco);

        /*        produto.setNomeProduto(jTextFieldNomeProduto.getText());
                produto.setDescricaoProduto(jTextFieldDescricaoProduto.getText());

                Double valorCompraDouble = jTextFieldValorCompra.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompraDouble);

                Double valorVendaDouble = jTextFieldValorVenda.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVendaDouble);

                Double valorQuantidadeEstoqueDouble = jTextFieldQuantidadeEstoque.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldQuantidadeEstoque.getText());
                produto.setQuantidadeEstoque(valorQuantidadeEstoqueDouble);*/

                pessoaFisica.setProduto(produto);

                telefones = new ArrayList<String>();
                for (int i = 0; i < telefonesListModel.size(); i++) {
                    telefones.add(telefonesListModel.getElementAt(i).toString());
                }
                pessoaFisica.setTelefone(telefones);
            }
            if(variavel == 2){
                
                if (novoRegistro == true){
                    pessoaJuridica = new PessoaJuridica(jTextFieldNome.getText(), jTextFieldCnpj.getText(), jTextFieldRazaoSocial.getText());
                }
                else{
                    pessoaJuridica.setNome(jTextFieldNome.getText());
                    pessoaJuridica.setCnpj(jTextFieldCnpj.getText());
                    pessoaJuridica.setRazaoSocial(jTextFieldRazaoSocial.getText());
                }
                
                endereco.setLogradouro(jTextFieldLogradouro.getText());
                endereco.setNumero(jTextFieldNumero.getText());
                endereco.setBairro(jTextFieldBairro.getText());
                endereco.setCidade(jTextFieldCidade.getText());
                endereco.setPais(jTextFieldPais.getText());
                endereco.setComplemento(jTextFieldComplemento.getText());

                pessoaJuridica.setEndereco(endereco);

          /*      produto.setNomeProduto(jTextFieldNomeProduto.getText());
                produto.setDescricaoProduto(jTextFieldDescricaoProduto.getText());

                Double valorCompraDouble = jTextFieldValorCompra.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompraDouble);

                Double valorVendaDouble = jTextFieldValorVenda.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVendaDouble);

                Double valorQuantidadeEstoqueDouble = jTextFieldQuantidadeEstoque.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldQuantidadeEstoque.getText());
                produto.setQuantidadeEstoque(valorQuantidadeEstoqueDouble);*/

                pessoaJuridica.setProduto(produto);

                telefones = new ArrayList<String>();
                for (int i = 0; i < telefonesListModel.size(); i++) {
                    telefones.add(telefonesListModel.getElementAt(i).toString());
                }
                pessoaJuridica.setTelefone(telefones);
            }
        

        if (novoRegistro == true) {
            if(variavel == 1)
                controleFornecedor.adicionar(pessoaFisica);
            
            if(variavel == 2)
                controleFornecedor.adicionar(pessoaJuridica);
        }
        
        modoAlteracao = false;
        novoRegistro = false;
        
        this.carregarListaFornecedores();
        this.habilitarDesabilitarCampos();
    }
    
    private void carregarListaFornecedores(){
        ArrayList<Fornecedor> listaFornecedores;
        listaFornecedores = controleFornecedor.getListaFornecedores();
        DefaultTableModel model = (DefaultTableModel) jTableListaFornecedores.getModel();
        model.setRowCount(0);

        for(Fornecedor fornecedor : listaFornecedores){
                if(fornecedor.getIndentificador().equalsIgnoreCase("pFisica")) {
                    PessoaFisica pf = (PessoaFisica) fornecedor;    
                    model.addRow(new String[]{pf.getNome(), pf.getCpf()});
                } else {
                    PessoaJuridica pj = (PessoaJuridica) fornecedor;
                    model.addRow(new String[]{pj.getNome(), pj.getCnpj()});
                }
        }
        
        jTableListaFornecedores.setModel(model);
    }
    
    private void pesquisarFornecedor(String nome) {
        Fornecedor fornecedorPesquisado = controleFornecedor.pesquisarFornecedor(nome);

        if (fornecedorPesquisado == null) {
            exibirInformacao("Fornecedor não encontrado.");
        } else {
            this.fornecedor= fornecedorPesquisado;
            this.preencherCampos();
            this.habilitarDesabilitarCampos();
        }
    }
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonNovo = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonPesquisar = new javax.swing.JButton();
        jLabelListaFornecedores = new javax.swing.JLabel();
        jScrollPaneListaFornecedores = new javax.swing.JScrollPane();
        jTableListaFornecedores = new javax.swing.JTable();
        jTabbedPaneInformacoes = new javax.swing.JTabbedPane();
        jPanelInformacoesGerais = new javax.swing.JPanel();
        jLabelNome = new javax.swing.JLabel();
        jLabelCpf = new javax.swing.JLabel();
        jLabelCnpj = new javax.swing.JLabel();
        jLabelRazaoSocial = new javax.swing.JLabel();
        jLabelTelefones = new javax.swing.JLabel();
        jScrollPaneListaTelefones = new javax.swing.JScrollPane();
        jListListaTelefones = new javax.swing.JList();
        jButtonAdicionarTelefone = new javax.swing.JButton();
        jButtonRemoverTelefone = new javax.swing.JButton();
        jTextFieldNome = new javax.swing.JTextField();
        jTextFieldCpf = new javax.swing.JTextField();
        jTextFieldCnpj = new javax.swing.JTextField();
        jTextFieldRazaoSocial = new javax.swing.JTextField();
        jLabelOpcaoFornecedor = new javax.swing.JLabel();
        jCheckBoxPessoaFisica = new javax.swing.JCheckBox();
        jCheckBoxPessoaJuridica = new javax.swing.JCheckBox();
        jPanelEndereco = new javax.swing.JPanel();
        jLabelLogradouro = new javax.swing.JLabel();
        jLabelNumero = new javax.swing.JLabel();
        jLabelBairro = new javax.swing.JLabel();
        jLabelCidade = new javax.swing.JLabel();
        jLabelEstado = new javax.swing.JLabel();
        jLabelPais = new javax.swing.JLabel();
        jLabelComplemento = new javax.swing.JLabel();
        jComboBoxEstado = new javax.swing.JComboBox();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jTextFieldNumero = new javax.swing.JTextField();
        jTextFieldBairro = new javax.swing.JTextField();
        jTextFieldCidade = new javax.swing.JTextField();
        jTextFieldPais = new javax.swing.JTextField();
        jTextFieldComplemento = new javax.swing.JTextField();
        jPanelProduto = new javax.swing.JPanel();
        jScrollPaneListaProdutos = new javax.swing.JScrollPane();
        jTableListaProdutos = new javax.swing.JTable();
        jButtonAdicionarProduto = new javax.swing.JButton();
        jButtonRemoverProduto = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Cadastro de Fornecedores");

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jButtonEditar.setText("Editar");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonPesquisar.setText("Pesquisar");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jLabelListaFornecedores.setText("Lista de fornecedores cadastrados:");

        jTableListaFornecedores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Fornecedor", "CPF/CNPJ"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTableListaFornecedores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaFornecedoresMouseClicked(evt);
            }
        });
        jScrollPaneListaFornecedores.setViewportView(jTableListaFornecedores);

        jLabelNome.setText("Nome:");

        jLabelCpf.setText("CPF:");

        jLabelCnpj.setText("CNPJ:");

        jLabelRazaoSocial.setText("Razão Social:");

        jLabelTelefones.setText("Telefones:");

        jListListaTelefones.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Telefone 1", "Telefone 2", "Telefone 3", "Telefone 4", "Telefone 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPaneListaTelefones.setViewportView(jListListaTelefones);

        jButtonAdicionarTelefone.setText("+");
        jButtonAdicionarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarTelefoneActionPerformed(evt);
            }
        });

        jButtonRemoverTelefone.setText("-");
        jButtonRemoverTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverTelefoneActionPerformed(evt);
            }
        });

        jTextFieldNome.setText("<Digite aqui o nome do fornecedor>");

        jTextFieldCpf.setText("<Digite aqui o CPF da Pessoa Física>");

        jTextFieldCnpj.setText("<Digite aqui o CNPJ da Pessoa Jurírica>");

        jTextFieldRazaoSocial.setText("<Digite aqui a Razão Social da Pessoa Jurídica>");

        jLabelOpcaoFornecedor.setText("Selecione a opção a qual deseja adicionar:");

        jCheckBoxPessoaFisica.setText("Pessoa Física");
        jCheckBoxPessoaFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPessoaFisicaActionPerformed(evt);
            }
        });

        jCheckBoxPessoaJuridica.setText("Pessoa Jurídica");
        jCheckBoxPessoaJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPessoaJuridicaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelInformacoesGeraisLayout = new javax.swing.GroupLayout(jPanelInformacoesGerais);
        jPanelInformacoesGerais.setLayout(jPanelInformacoesGeraisLayout);
        jPanelInformacoesGeraisLayout.setHorizontalGroup(
            jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInformacoesGeraisLayout.createSequentialGroup()
                .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInformacoesGeraisLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelRazaoSocial)
                            .addComponent(jLabelCnpj)
                            .addComponent(jLabelCpf)
                            .addComponent(jLabelNome)
                            .addComponent(jLabelTelefones))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldNome, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldCpf)
                            .addComponent(jTextFieldCnpj)
                            .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                            .addComponent(jScrollPaneListaTelefones))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonAdicionarTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonRemoverTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanelInformacoesGeraisLayout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabelOpcaoFornecedor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBoxPessoaFisica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBoxPessoaJuridica)))
                .addContainerGap(265, Short.MAX_VALUE))
        );
        jPanelInformacoesGeraisLayout.setVerticalGroup(
            jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInformacoesGeraisLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelOpcaoFornecedor)
                    .addComponent(jCheckBoxPessoaFisica)
                    .addComponent(jCheckBoxPessoaJuridica))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNome)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCpf)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCnpj)
                    .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelRazaoSocial)
                    .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelTelefones)
                    .addGroup(jPanelInformacoesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelInformacoesGeraisLayout.createSequentialGroup()
                            .addComponent(jButtonAdicionarTelefone)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jButtonRemoverTelefone))
                        .addComponent(jScrollPaneListaTelefones, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPaneInformacoes.addTab("Informações Gerais", jPanelInformacoesGerais);

        jLabelLogradouro.setText("Logradouro:");

        jLabelNumero.setText("Número:");

        jLabelBairro.setText("Bairro:");

        jLabelCidade.setText("Cidade:");

        jLabelEstado.setText("Estado:");

        jLabelPais.setText("País:");

        jLabelComplemento.setText("Complemento:");

        jComboBoxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC \t", "AL \t", "AP \t", "AM \t", "BA \t", "CE \t", "DF \t", "ES \t", "GO \t", "MA \t", "MT \t", "MS \t", "MG \t", "PA \t", "PB \t", "PR \t", "PE \t", "PI \t", "RJ \t", "RN \t", "RS \t", "RO \t", "RR \t", "SC \t", "SP \t", "SE \t", "TO" }));

        jTextFieldLogradouro.setText("<Digite aqui o Logradouro onde reside o fornecedor>");

        jTextFieldNumero.setText("<Digite aqui o número onde reside o fornecedor>");

        jTextFieldBairro.setText("<Digite aqui o bairro onde reside o fornecedor>");

        jTextFieldCidade.setText("<Digite aqui a cidade onde reside o fornecedor>");

        jTextFieldPais.setText("<Digite aqui o país onde reside o fornecedor>");

        jTextFieldComplemento.setText("<Digite aqui o complemento de onde reside o fornecedor>");

        javax.swing.GroupLayout jPanelEnderecoLayout = new javax.swing.GroupLayout(jPanelEndereco);
        jPanelEndereco.setLayout(jPanelEnderecoLayout);
        jPanelEnderecoLayout.setHorizontalGroup(
            jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelComplemento)
                    .addComponent(jLabelPais)
                    .addComponent(jLabelEstado)
                    .addComponent(jLabelCidade)
                    .addComponent(jLabelBairro)
                    .addComponent(jLabelNumero)
                    .addComponent(jLabelLogradouro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                        .addComponent(jTextFieldNumero)
                        .addComponent(jTextFieldBairro)
                        .addComponent(jTextFieldCidade)
                        .addComponent(jTextFieldPais)
                        .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(335, Short.MAX_VALUE))
        );
        jPanelEnderecoLayout.setVerticalGroup(
            jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelLogradouro)
                    .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNumero)
                    .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelBairro)
                    .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCidade)
                    .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEstado)
                    .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPais)
                    .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelComplemento)
                    .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(54, Short.MAX_VALUE))
        );

        jTabbedPaneInformacoes.addTab("Endereço", jPanelEndereco);

        jTableListaProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Nome", "Valor de Compra", "Valor de Venda", "Quantidade em Estoque"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPaneListaProdutos.setViewportView(jTableListaProdutos);

        jButtonAdicionarProduto.setText("Adicionar");
        jButtonAdicionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarProdutoActionPerformed(evt);
            }
        });

        jButtonRemoverProduto.setText("Remover");
        jButtonRemoverProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverProdutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelProdutoLayout = new javax.swing.GroupLayout(jPanelProduto);
        jPanelProduto.setLayout(jPanelProdutoLayout);
        jPanelProdutoLayout.setHorizontalGroup(
            jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPaneListaProdutos)
                .addContainerGap())
            .addGroup(jPanelProdutoLayout.createSequentialGroup()
                .addGap(325, 325, 325)
                .addComponent(jButtonAdicionarProduto)
                .addGap(18, 18, 18)
                .addComponent(jButtonRemoverProduto)
                .addContainerGap(333, Short.MAX_VALUE))
        );
        jPanelProdutoLayout.setVerticalGroup(
            jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProdutoLayout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonAdicionarProduto)
                    .addComponent(jButtonRemoverProduto))
                .addGap(65, 65, 65)
                .addComponent(jScrollPaneListaProdutos, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        jTabbedPaneInformacoes.addTab("Produto", jPanelProduto);

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTabbedPaneInformacoes)
                    .addComponent(jScrollPaneListaFornecedores)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonNovo)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonExcluir)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonEditar)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonPesquisar))
                    .addComponent(jLabelListaFornecedores))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonSalvar)
                .addGap(18, 18, 18)
                .addComponent(jButtonCancelar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonNovo)
                    .addComponent(jButtonExcluir)
                    .addComponent(jButtonPesquisar)
                    .addComponent(jButtonEditar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelListaFornecedores)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPaneListaFornecedores, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPaneInformacoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSalvar)
                    .addComponent(jButtonCancelar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed
        fornecedor = null;
        modoAlteracao = true;
        novoRegistro = true;
        this.limparCampos();
        this.habilitarDesabilitarCampos();
        this.jTextFieldNome.requestFocus();
    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        this.salvarRegistro();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        if (novoRegistro == true) {
            this.limparCampos();
        } else {
            this.preencherCampos();
        }
        modoAlteracao = false;
        novoRegistro = false;
        this.habilitarDesabilitarCampos();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jCheckBoxPessoaFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPessoaFisicaActionPerformed
        jTextFieldCnpj.setEnabled(modoAlteracao == false);
        jTextFieldRazaoSocial.setEnabled(modoAlteracao == false);
        jTextFieldCpf.setEnabled(modoAlteracao == true);
        variavel = 1;
        
    }//GEN-LAST:event_jCheckBoxPessoaFisicaActionPerformed

    private void jCheckBoxPessoaJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPessoaJuridicaActionPerformed
        jTextFieldCpf.setEnabled(modoAlteracao == false);
        jTextFieldCnpj.setEnabled(modoAlteracao == true);
        jTextFieldRazaoSocial.setEnabled(modoAlteracao == true);
        variavel = 2;
    }//GEN-LAST:event_jCheckBoxPessoaJuridicaActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
        if(this.fornecedor instanceof PessoaFisica){
            this.pessoaFisica = (PessoaFisica) this.fornecedor;
            this.controleFornecedor.remover(this.pessoaFisica);
        }
        else if(this.fornecedor instanceof PessoaJuridica){
            this.pessoaJuridica = (PessoaJuridica) this.fornecedor;
            this.controleFornecedor.remover(this.pessoaFisica);
        }
        
        this.fornecedor = null;
        this.limparCampos();
        this.carregarListaFornecedores();
        this.habilitarDesabilitarCampos();
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jButtonAdicionarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarTelefoneActionPerformed
    CadastroTelefone cadastroTel;
    cadastroTel = new CadastroTelefone(this, true);
    cadastroTel.setVisible(true);
    cadastroTel.setLocationRelativeTo(null);
    if (cadastroTel.getTelefone() != null) {
        telefonesListModel.addElement(cadastroTel.getTelefone());
    }
    cadastroTel.dispose();
    }//GEN-LAST:event_jButtonAdicionarTelefoneActionPerformed

    private void jButtonRemoverTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverTelefoneActionPerformed
        if (jListListaTelefones.getSelectedIndex() != -1) {
            telefonesListModel.removeElementAt(jListListaTelefones.getSelectedIndex());
        }
    }//GEN-LAST:event_jButtonRemoverTelefoneActionPerformed

    private void jTableListaFornecedoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaFornecedoresMouseClicked
        if (jTableListaFornecedores.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableListaFornecedores.getModel();
            String nome = (String) model.getValueAt(jTableListaFornecedores.getSelectedRow(), 0);
            this.fornecedor = this.controleFornecedor.pesquisarFornecedor(nome);
            jButtonExcluir.setEnabled(true);
            jButtonEditar.setEnabled(true);
        }
    }//GEN-LAST:event_jTableListaFornecedoresMouseClicked

    private void jButtonAdicionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarProdutoActionPerformed
    CadastroProduto cadastroP;
    cadastroP = new CadastroProduto(this, true);
    cadastroP.setVisible(true);
    cadastroP.setLocationRelativeTo(null);
    
    
      ArrayList<Produto> listaProdutos;
      listaProdutos = controleProduto.getListaProdutos();
      DefaultTableModel model = (DefaultTableModel) jTableListaProdutos.getModel();
      model.setRowCount(0);
     
   
        model.addRow(new String[]{cadastroP.getNomeProduto()});
        
        jTableListaProdutos.setModel(model);
        
        /*private void carregarListaProdutos(){
        ArrayList<Produto> listaProdutos;
        listaProdutos = controleProduto.getListaProdutos();
        DefaultTableModel model = (DefaultTableModel) jTableListaProdutos.getModel();
        model.setRowCount(0);
        CadastroProduto cadastroP;
        cadastroP = new CadastroProduto(this, true);
        
        for (Produto umProduto : listaProdutos){
            model.addRow(new String[]{produto.getNomeProduto(), produto.get(cadastro.getValorCompra()});
        }
        jTableListaProdutos.setModel(model);
    }*/
        
    cadastroP.dispose();
    }//GEN-LAST:event_jButtonAdicionarProdutoActionPerformed

    private void jButtonRemoverProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverProdutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonRemoverProdutoActionPerformed

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
        String pesquisa = JOptionPane.showInputDialog("Informe o nome do Fornecedor:");
    if (pesquisa != null) {
        this.pesquisarFornecedor(pesquisa);
    }
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        modoAlteracao = true;
        novoRegistro = false;
        this.habilitarDesabilitarCampos();
        this.jTextFieldNome.requestFocus();
    }//GEN-LAST:event_jButtonEditarActionPerformed

 
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroFornecedor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarProduto;
    private javax.swing.JButton jButtonAdicionarTelefone;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonRemoverProduto;
    private javax.swing.JButton jButtonRemoverTelefone;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JCheckBox jCheckBoxPessoaFisica;
    private javax.swing.JCheckBox jCheckBoxPessoaJuridica;
    private javax.swing.JComboBox jComboBoxEstado;
    private javax.swing.JLabel jLabelBairro;
    private javax.swing.JLabel jLabelCidade;
    private javax.swing.JLabel jLabelCnpj;
    private javax.swing.JLabel jLabelComplemento;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelEstado;
    private javax.swing.JLabel jLabelListaFornecedores;
    private javax.swing.JLabel jLabelLogradouro;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNumero;
    private javax.swing.JLabel jLabelOpcaoFornecedor;
    private javax.swing.JLabel jLabelPais;
    private javax.swing.JLabel jLabelRazaoSocial;
    private javax.swing.JLabel jLabelTelefones;
    private javax.swing.JList jListListaTelefones;
    private javax.swing.JPanel jPanelEndereco;
    private javax.swing.JPanel jPanelInformacoesGerais;
    private javax.swing.JPanel jPanelProduto;
    private javax.swing.JScrollPane jScrollPaneListaFornecedores;
    private javax.swing.JScrollPane jScrollPaneListaProdutos;
    private javax.swing.JScrollPane jScrollPaneListaTelefones;
    private javax.swing.JTabbedPane jTabbedPaneInformacoes;
    private javax.swing.JTable jTableListaFornecedores;
    private javax.swing.JTable jTableListaProdutos;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCnpj;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldPais;
    private javax.swing.JTextField jTextFieldRazaoSocial;
    // End of variables declaration//GEN-END:variables

    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }


}
