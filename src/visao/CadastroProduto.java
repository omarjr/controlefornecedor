

package visao;

import control.ControleProduto;
import model.Produto;

/**
 *
 * @author omarfsjunior
 */
public class CadastroProduto extends javax.swing.JDialog {
    
    private String nomeProduto;
    private String descricaoProduto;
    private Double valorCompra;
    private Double valorVenda;
    private Double quantidadeEstoque;
    private Produto produto;
    private ControleProduto controleProduto;
    
    
    
    public String getNomeProduto() {
        return this.nomeProduto;
    }
    public String getdescricaoProduto() {
        return this.descricaoProduto;
    }
    public Double getValorCompra() {
        return this.valorCompra;
    }
    public Double getValorVenda() {
        return this.valorVenda;
    }
    public Double getQuantidadeEstoque() {
        return this.quantidadeEstoque;
    }
    

    public CadastroProduto(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.produto = new Produto();
        this.controleProduto = new ControleProduto();
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelNomeProduto = new javax.swing.JLabel();
        jLabelDescricaoProduto = new javax.swing.JLabel();
        jLabelValorCompra = new javax.swing.JLabel();
        jLabelValorVenda = new javax.swing.JLabel();
        jLabelQuantidadeEstoque = new javax.swing.JLabel();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jTextFieldValorCompra = new javax.swing.JTextField();
        jTextFieldValorVenda = new javax.swing.JTextField();
        jTextFieldQuantidadeEstoque = new javax.swing.JTextField();
        jScrollPaneDescricaoProduto = new javax.swing.JScrollPane();
        jTextAreaDescricaoProduto = new javax.swing.JTextArea();
        jButtonAdicionarProduto = new javax.swing.JButton();
        jButtonCancelarProduto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Produto");

        jLabelNomeProduto.setText("Nome:");

        jLabelDescricaoProduto.setText("Descrição:");

        jLabelValorCompra.setText("Valor de Compra:");

        jLabelValorVenda.setText("Valor de Venda:");

        jLabelQuantidadeEstoque.setText("Quantidade em Estoque:");

        jTextFieldValorCompra.setText("0.00");

        jTextFieldValorVenda.setText("0.00");

        jTextAreaDescricaoProduto.setColumns(20);
        jTextAreaDescricaoProduto.setRows(5);
        jScrollPaneDescricaoProduto.setViewportView(jTextAreaDescricaoProduto);

        jButtonAdicionarProduto.setText("Adicionar");
        jButtonAdicionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarProdutoActionPerformed(evt);
            }
        });

        jButtonCancelarProduto.setText("Cancelar");
        jButtonCancelarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarProdutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(121, 121, 121)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabelDescricaoProduto)
                                    .addComponent(jLabelNomeProduto)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jLabelQuantidadeEstoque))
                            .addComponent(jLabelValorCompra, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelValorVenda, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldValorVenda)
                            .addComponent(jTextFieldQuantidadeEstoque)
                            .addComponent(jTextFieldValorCompra)
                            .addComponent(jScrollPaneDescricaoProduto)
                            .addComponent(jTextFieldNomeProduto))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonAdicionarProduto)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonCancelarProduto)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNomeProduto)
                    .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelDescricaoProduto)
                    .addComponent(jScrollPaneDescricaoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelValorCompra)
                    .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelValorVenda)
                    .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldQuantidadeEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelQuantidadeEstoque))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancelarProduto)
                    .addComponent(jButtonAdicionarProduto))
                .addGap(12, 12, 12))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAdicionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarProdutoActionPerformed
        this.nomeProduto = jTextFieldNomeProduto.getText();
        this.descricaoProduto = jTextAreaDescricaoProduto.getText();
        Double valorCompraDouble = jTextFieldValorCompra.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorCompra.getText());
        this.valorCompra = valorCompraDouble;
        Double valorVendaDouble = jTextFieldValorVenda.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorVenda.getText());
        this.valorVenda = valorVendaDouble;
        Double valorQuantidadeEstoqueDouble = jTextFieldQuantidadeEstoque.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldQuantidadeEstoque.getText());
        this.quantidadeEstoque = valorQuantidadeEstoqueDouble;
      
        
        this.setVisible(false);
    }//GEN-LAST:event_jButtonAdicionarProdutoActionPerformed

    private void jButtonCancelarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarProdutoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonCancelarProdutoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                CadastroProduto dialog = new CadastroProduto(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdicionarProduto;
    private javax.swing.JButton jButtonCancelarProduto;
    private javax.swing.JLabel jLabelDescricaoProduto;
    private javax.swing.JLabel jLabelNomeProduto;
    private javax.swing.JLabel jLabelQuantidadeEstoque;
    private javax.swing.JLabel jLabelValorCompra;
    private javax.swing.JLabel jLabelValorVenda;
    private javax.swing.JScrollPane jScrollPaneDescricaoProduto;
    private javax.swing.JTextArea jTextAreaDescricaoProduto;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldQuantidadeEstoque;
    private javax.swing.JTextField jTextFieldValorCompra;
    private javax.swing.JTextField jTextFieldValorVenda;
    // End of variables declaration//GEN-END:variables
}
