package model;

import java.util.ArrayList;

public class Fornecedor {
    
    protected String nome;
    protected ArrayList<String> telefones;
    private Endereco endereco;
    private ArrayList<Produto> produto;
    protected String identificador;
    
    
    public Fornecedor(){
        produto = new ArrayList<Produto>();
        identificador = "Fornecedor";
    }

    public Fornecedor (String nome){
        this.nome = nome;
        produto = new ArrayList<Produto>();
        identificador = "Fornecedor";
        
}
    public String getIndentificador(){
        return identificador;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Produto> getProdutos() {
        return produto;
    }

       
    public void setProduto(Produto umProduto) {
        this.produto.add(umProduto);
    }

    public ArrayList<String> getTelefone() {
        return telefones;
    }

    public void setTelefone(ArrayList<String> telefone) {
        this.telefones = telefone;
    }
    
    
}
