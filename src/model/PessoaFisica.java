
package model;

public class PessoaFisica extends Fornecedor{
    
    private String cpf;
    
      public PessoaFisica() {
        this.identificador = "pFisica";
    }
    
    
    public PessoaFisica(String nome, String cpf) {
        super(nome);
        this.cpf = cpf;
        this.identificador = "pFisica";
    }

    

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    

}