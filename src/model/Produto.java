
package model;

public class Produto {
    
    private String nome;
    private String descricao;
    private double valorCompra;
    private double valorVenda;
    private double quantidadeEstoque;

    public String getDescricaoProduto() {
        return descricao;
    }

    public void setDescricaoProduto(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeProduto() {
        return nome;
    }

    public void setNomeProduto(String nome) {
        this.nome = nome;
    }

    public double getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(double quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }

    public double getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    public double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(double valorVenda) {
        this.valorVenda = valorVenda;
    }

        
}
