
package control;

import java.util.ArrayList;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import model.Produto;
import visao.CadastroProduto;

public class ControleProduto {
    private ArrayList <Produto> listaProdutos;
    
    
    public ControleProduto(){
        listaProdutos = new ArrayList<Produto>();
     
}
    
        public String adicionar(Produto produto){
            listaProdutos.add(produto);
            return "Produto adicionado com sucesso!";
        }
        
        public String remover(Produto produto){
            listaProdutos.add(produto);
            return "Produto removido com sucesso!";
        }
    
        public Produto pesquisar(String nome) {
            for (Produto mercadoria: listaProdutos) {
                if (mercadoria.getNomeProduto().equalsIgnoreCase(nome)) return mercadoria;
            }
            return null;
        }

    public ArrayList<Produto> getListaProdutos() {
        return listaProdutos;
    }
     
   
}
