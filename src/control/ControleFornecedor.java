
package control;

import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import java.util.ArrayList;

public class ControleFornecedor {
    
    private ArrayList <Fornecedor> listaFornecedores;
    
    public ControleFornecedor(){
        listaFornecedores = new ArrayList<Fornecedor>();
     
}
        public String adicionar(PessoaFisica fornecedor){
            listaFornecedores.add((Fornecedor)fornecedor);
            return "Fornecedor Pessoa Física adicionado com sucesso!";
        }
        
        public String adicionar(PessoaJuridica fornecedor){
            listaFornecedores.add((Fornecedor)fornecedor);
            return "Fornecedor Pessoa Jurídica adicionado com sucesso!";
        }
        
        public String remover(PessoaFisica fornecedor) {
            listaFornecedores.remove(fornecedor);
            return "Fornecedor Pessoa Física removida com Sucesso!";
        }
        
        public String remover(PessoaJuridica fornecedor) {
            listaFornecedores.remove(fornecedor);
            return "Fornecedor Pessoa Juridica removida com Sucesso!";
        }
    
        public Fornecedor pesquisarFornecedor(String nome) {
            for (Fornecedor fornecedor: listaFornecedores) {
                if (fornecedor.getNome().equalsIgnoreCase(nome)) return fornecedor;
            }
            return null;
        }


    public ArrayList<Fornecedor> getListaFornecedores() {
        return listaFornecedores;
    }
}
